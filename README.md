# workshop-gitlab-ci

![pipeline](../badges/main/pipeline.svg?ignore_skipped=true)
![coverage](../badges/main/coverage.svg?job=test)

## How to run Demo Project

- Install PostgreSQL

```bash
make pgu
```

- Migrate Database

```bash
make mgu
```

- Rename file .env.example to .env

```text
DB_URI=postgresql://postgres:pgpwd@localhost:54321/demo-db
```

- Install Dependencies

```bash
npm ci
```

- Run Demo Project

```bash
npm start
```

- Go to <http://localhost:3000>

- Run Unit Test

```bash
npm run test:unit
```

- Run Integration Test

```bash
npm run test:integration
```

- Run All Test

```bash
npm run test
```

- Run All Test with converage

```bash
npm run test:coverage
```

## How to build docker image

```bash
docker image build -t registry.gitlab.com/your_username/workshop-gitlab-ci .
```

## How to Deploy

- Change `registry.gitlab.com/somprasongd/workshop-gitlab-ci` to `registry.gitlab.com/your_username/workshop-gitlab-ci`

```diff
  app:
    build: .
-   image: registry.gitlab.com/somprasongd/workshop-gitlab-ci
+   image: registry.gitlab.com/your_username/workshop-gitlab-ci
    container_name: demo-app
```

- Run `docker compose up -d`

## How create CI/CD Pipeline

### Step 1: Create .gitlab-ci.yml

```bash
touch .gitlab-ci.yml
```

### Step 2: Create jobs

- Create branch dev, run `git checkout -b dev`

- Update `.gitlab-ci.yml`

```yml
test:unit:
  script:
    - echo "Running unit testing script"

test:integration:
  script:
    - echo "Running integration testing script"

test:converage:
  script:
    - echo "Running converage testing script"

build:dev:
  script:
    - echo "Running build job on dev branch"
    - echo "Running build docker image script"
    - echo "Running push docker image to registry script"
  only:
    - dev

deploy:dev:
  script:
    - echo "Running deploy job on dev branch"
    - echo "Running deploy script"
  only:
    - dev

build:staging:
  script:
    - echo "Running build job on main branch"
    - echo "Running build docker image script"
    - echo "Running push docker image to registry script"
  only:
    - main

deploy:staging:
  script:
    - echo "Running deploy job on main branch"
    - echo "Running deploy script"
  only:
    - main

build:prod:
  script:
    - echo "Running build job when tags"
    - echo "Running build docker image script"
    - echo "Running push docker image to registry script"
  only:
    - tags

deploy:prod:
  script:
    - echo "Running deploy job when tags"
    - echo "Running deploy script"
  only:
    - tags
```

- Commit and push dev branch

```bash
git add .
git commit -m "add gitlab-ci file"
git push origin dev
```

- Go to CI/CD --> Pipelines

![step2](./resources/step2.png)

### Step 3: Group with stage

- Update `.gitlab-ci.yml` to add stage in each jobs.

```diff
test:unit:
+ stage: test
  script:

test:integration:
+ stage: test
  script:

test:converage:
+ stage: test
  script:

build:dev:
+ stage: build
  script:

deploy:dev:
+ stage: deploy
  script:

build:staging:
+ stage: build
  script:

deploy:staging:
+ stage: deploy
  script:

build:prod:
+ stage: build
  script:

deploy:prod:
+ stage: deploy
  script:
```

- Commit and push dev branch

![step3](./resources/step3.png)

### Step 4: Run in sequence with stages management

- Update `.gitlab-ci.yml`

```yml
# Add on the first line
stages:
  - test
  - build
  - deploy
```

- Commit and push dev branch

![step4](./resources/step4.png)

### Step 5: Run the unit test

```yml
stages:
  - test
  - build
  - deploy

# This folder is cached between builds
cache:
  paths:
    - node_modules/

test:unit:
  stage: test
  image: node:16-alpine
  before_script:
    - npm ci
  script:
    - npm run test:unit
```

- Commit and push dev branch

![step5](./resources/step5.png)

![step5-fail](./resources/step5-1.png)

### Step 6: Run the integration test

Integration test required to use PostgreSQL database, you can use service postgres with image like this:

```yml
test:integration:
  stage: test
  # Use
  services:
    - postgres:15-alpine
  image: node:16-alpine
  variables:
    POSTGRES_DB: test-db
    POSTGRES_USER: pguser
    POSTGRES_PASSWORD: pgpassword
    POSTGRES_HOST_AUTH_METHOD: trust
    DB_URI: 'postgres://pguser:pgpassword@postgres:5432/test-db'
  before_script:
    - apk add --update curl
    # Download migrate app
    - curl -L https://github.com/golang-migrate/migrate/releases/download/v4.15.2/migrate.linux-amd64.tar.gz | tar xvz
    # Run migrate up
    - ./migrate -verbose -path=./migrations/ -database "postgresql://pguser:pgpassword@postgres:5432/test-db?sslmode=disable" up
    # Install node.js dependencies
    - npm ci
  script:
    - npm run test:integration
```

- Commit and push dev branch

![step6](./resources/step6.png)

### Step 7: Coverage test and capture it

`npm run test:coverage` it run all unit and integration tests, you can do ignore "test:unit", "test:integration" and "test:converage" jobs to use a single "test" job like this:

```yml
test:
  stage: test
  services:
    - postgres:15-alpine
  image: node:16-alpine
  variables:
    POSTGRES_DB: test-db
    POSTGRES_USER: pguser
    POSTGRES_PASSWORD: pgpassword
    POSTGRES_HOST_AUTH_METHOD: trust
    DB_URI: 'postgres://$POSTGRES_USER:$POSTGRES_PASSWORD@postgres:5432/$POSTGRES_DB?sslmode=disable'
  before_script:
    - apk add --update curl
    # Download migrate app
    - curl -L https://github.com/golang-migrate/migrate/releases/download/v4.15.2/migrate.linux-amd64.tar.gz | tar xvz
    # Run migrate up
    - ./migrate -verbose -path=./migrations/ -database "$DB_URI" up
    # Install node.js dependencies
    - npm ci
  script:
    - npm run test:coverage
  coverage: '/^All files\s+\|\s+\d+\.*\d*\s+\|\s*(\d+\.*\d*)/'
  artifacts:
    paths:
      - coverage/
    expire_in: 1 week
```

- Commit and push dev branch

![step7](./resources/step7.png)

### Step 8: Build to docker image and push it to gitlab registry

- Update "build:dev" job to build `dev` image

```yml
build:dev:
  stage: build
  image: docker:latest
  services:
    - docker:dind
  before_script:
    # Login to gitlab registry with your gitlab user
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    # Build docker image with dev tag
    - echo "Build docker image from $CI_COMMIT_REF_SLUG, commit $CI_COMMIT_SHORT_SHA"
    # Step 1: Build new image with "dev" tag
    - docker build --pull -t "$CI_REGISTRY_IMAGE":dev .
    # Step 2: Push new image to docker registry
    - docker push "$CI_REGISTRY_IMAGE":dev
  only:
    - dev
```

- Update "build:staging" job to build `latest` image

```yml
build:staging:
  stage: build
  image: docker:latest
  services:
    - docker:dind
  before_script:
    # Login to gitlab registry with your gitlab user
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    # Build docker image with latest tag
    - echo "Build docker image from main"
    # Step 1: Build new image with "latest" tag
    - docker build --pull -t "$CI_REGISTRY_IMAGE" .
    # Step 2: Push new image to docker registry
    - docker push "$CI_REGISTRY_IMAGE"
  only:
    - main
```

- Update "build:prod" job to build `tag version` image

```yml
build:prod:
  stage: build
  image: docker:latest
  services:
    - docker:dind
  before_script:
    # Login to gitlab registry with your gitlab user
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - echo "Build docker image from $CI_COMMIT_TAG"
    # Step 1: Pull latest image from registry
    - docker image pull "$CI_REGISTRY_IMAGE":latest
    # Step 2: Tag new version from "latest" image
    - docker image tag "$CI_REGISTRY_IMAGE":latest "$CI_REGISTRY_IMAGE":$CI_COMMIT_TAG
    # Step 3: Push new image to docker registry
    - docker image push "$CI_REGISTRY_IMAGE":$CI_COMMIT_TAG
  only:
    - tags
```

- Commit and push dev branch

![step8](./resources/step8.png)

### Step 9: Deploy to dev server

- Before start go to Settings > CI/CD > Vaiables and create new viriables:

  - `DEV_SSH_HOST`: IP or Domain of Dev Server
  - `DEV_SSH_PORT`: Port of Dev Server
  - `DEV_SSH_USER`: ssh username
  - `DEV_SSH_PRIVATE_KEY`: ssh private key to login to dev server

  See [How to run dev server in your local machine](#how-to-run-dev-server-in-your-local-machine)

- Update "deploy:dev" job

```yml
deploy:dev:
  stage: deploy
  image: alpine:3.17
  variables:
    APP_DIR: ~/app
    SSH_USER_SERVER: $DEV_SSH_USER@$DEV_SSH_HOST
    SSH_SERVER_PORT: $DEV_SSH_PORT
    IMAGE_VERSION: dev
  before_script:
    - apk add --update openssh-client
    - eval $(ssh-agent -s)
    - echo "$DEV_SSH_PRIVATE_KEY" | ssh-add -
    - mkdir -p ~/.ssh
    - ssh-keyscan -p $DEV_SSH_PORT $DEV_SSH_HOST >> ~/.ssh/known_hosts
  script:
    # Step 1: Create app directory "~/app"
    - ssh $SSH_USER_SERVER -p $SSH_SERVER_PORT mkdir -p $APP_DIR
    # Step 2: Copy docker-compose.yml to ~/app/docker-compose.yml
    - scp -P $SSH_SERVER_PORT -o stricthostkeychecking=no -r ./docker-compose.yml $SSH_USER_SERVER:$APP_DIR/docker-compose.yml
    # Step 3: Login to gitlab registry
    - ssh $SSH_USER_SERVER -p $SSH_SERVER_PORT docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    # Step 4: Pull new image
    - ssh $SSH_USER_SERVER -p $SSH_SERVER_PORT "cd $APP_DIR&&IMAGE_VERSION=$IMAGE_VERSION docker compose pull"
    # Step 5: Deploy
    - ssh $SSH_USER_SERVER -p $SSH_SERVER_PORT "cd $APP_DIR&&IMAGE_VERSION=$IMAGE_VERSION docker compose up -d"
    # Step 6: Clean up (remove unused images)
    - ssh $SSH_USER_SERVER -p $SSH_SERVER_PORT "docker image prune -f"
  only:
    - dev
```

- Commit and push dev branch

![step9](./resources/step9.png)

## How to run dev server in your local machine

- Run Ubuntu Server in Docker

```bash
docker run -v /var/run/docker.sock:/var/run/docker.sock --privileged -d -p 22:22 --name=demo-svr somprasongd/demo-svr-ssh:22.04
```

Use this private key to login with user `ost`

```bash
-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAAAMwAAAAtzc2gtZW
QyNTUxOQAAACBa2OFvTpN+lcU3ocJaqF+EgDT57uX53VqNB48eh01zPgAAAJBsG0EDbBtB
AwAAAAtzc2gtZWQyNTUxOQAAACBa2OFvTpN+lcU3ocJaqF+EgDT57uX53VqNB48eh01zPg
AAAEDogHc4HsRpXPO07iceEJhFA9njY0w4jCHXOyW4bv0s01rY4W9Ok36VxTehwlqoX4SA
NPnu5fndWo0Hjx6HTXM+AAAADG9zdEBkZW1vLXN2cgE=
-----END OPENSSH PRIVATE KEY-----

```

- Run Ngrok

  - Install [Ngrok](https://ngrok.com/download)
  - Add authtoken

  ```bash
  ngrok config add-authtoken <token>
  ```

  - Start tunnel on port 22

  ```bash
  ngrok tcp 22

  Check which logged users are accessing your tunnels in real time https://ngrok.com/s/app-users

  Session Status  online
  Account         Somprasong Damyos (Plan: Free)
  Version         3.1.1
  Region          Asia Pacific (ap)
  Latency         56ms
  Web Interface   http://127.0.0.1:4040
  Forwarding      tcp://0.tcp.ap.ngrok.io:18224 -> localhost:22
  Connections ttl opn rt1   rt5   p50   p90
              113 0   0.00  0.00  2.83  125.69
  ```

  - From example domain is `0.tcp.ap.ngrok.io` and port is `18224`

- Test ssh to server

```bash
ssh ost@0.tcp.ap.ngrok.io -p 18224

ost@0.tcp.ap.ngrok.io's password: ost123
```

- Test run docker container

```bash
ost@b7e08f3324ec:~$ docker run --rm hello-world
```

- If error

```bash
docker: Got permission denied while trying to connect to the Docker daemon socket at unix:///var/run/docker.sock: Post http://%2Fvar%2Frun%2Fdocker.sock/v1.35/containers/create: dial unix /var/run/docker.sock: connect: permission denied.
See 'docker run --help'.`
```

Run `sudo chmod 666 /var/run/docker.sock` to fix that error.

```bash
ost@b7e08f3324ec:~$ sudo chmod 666 /var/run/docker.sock
ost@b7e08f3324ec:~$ docker run --rm hello-world
```
